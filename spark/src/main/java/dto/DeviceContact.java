package dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class DeviceContact implements Serializable {
    private String id1;
    private String id2;
}
