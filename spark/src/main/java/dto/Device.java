package dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.util.Map;

@Data
@AllArgsConstructor
public class Device implements Serializable {
    private String ip;
    private String id;

    public Device(Map<String, Object> map) {
        ip = (String) map.get("ip");
        id = (String) map.get("id");
    }

    public Device(String string) {

    }
}
