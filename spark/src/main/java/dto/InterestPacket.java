package dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class InterestPacket implements Serializable {
    private String ip;
    private String id;

    public InterestPacket(String rawPacket) {
        if(rawPacket.charAt(0) != 'i') {
            throw new IllegalArgumentException("Invalid Packet");
        }
        String[] components = rawPacket.split(",", 2);
        ip = components[1];
        id = getIdFromIp(ip);
    }

    private String getIdFromIp(String ip) {
        return ip.split("::", 2)[1].replace(":", "-");
    }
}
