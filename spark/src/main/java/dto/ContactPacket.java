package dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class ContactPacket implements Serializable {
    private String ip1;
    private String ip2;
    private String id1;
    private String id2;

    public ContactPacket(String rawPacket) {
        if(rawPacket.charAt(0) != 'c') {
            throw new IllegalArgumentException("Invalid Packet");
        }
        String[] components = rawPacket.split(",", 3);
        ip1 = components[1];
        ip2 = components[2];
        id1 = getIdFromIp(ip1);
        id2 = getIdFromIp(ip2);
    }

    private String getIdFromIp(String ip) {
        return ip.split("::", 2)[1].replace(":", "-");
    }
}
