import dto.ContactPacket;
import dto.Device;
import dto.DeviceContact;
import dto.InterestPacket;
import org.apache.spark.SparkConf;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaReceiverInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import util.Neo4jUtils;
import util.SocketUtils;

import java.util.List;
import java.util.function.Consumer;

public class ContactTracingApp {
    private static final String MASTER_URL = "spark://spark-master:7077";

    public static void main(String[] args) throws InterruptedException {
        SparkConf conf = new SparkConf().setMaster(MASTER_URL).setAppName("Contact Tracing");
        JavaStreamingContext jssc = new JavaStreamingContext(conf, Durations.seconds(10));

        JavaReceiverInputDStream<String> lines = jssc.socketTextStream("bridge", 1235);

        JavaDStream<String> contactLines = lines.filter(e -> e.length() > 0 && e.charAt(0) == 'c');
        JavaDStream<String> interestLines = lines.filter(e -> e.length() > 0 && e.charAt(0) == 'i');

        handleContacts(contactLines);
        handleInterest(interestLines);

        jssc.start();
        jssc.awaitTermination();

        Neo4jUtils.closeDriver();
    }

    private static void handleContacts(JavaDStream<String> contactLines) {
        JavaDStream<ContactPacket> contacts = contactLines.map(ContactPacket::new);

        JavaDStream<Device> devices =
                contacts.flatMap(
                        e ->
                                List.of(
                                                new Device(e.getIp1(), e.getId1()),
                                                new Device(e.getIp2(), e.getId2()))
                                        .iterator());
        forEach(devices, Neo4jUtils::createDeviceNode);

        JavaDStream<DeviceContact> deviceContacts =
                contacts.map(p -> new DeviceContact(p.getId1(), p.getId2()));
        forEach(deviceContacts, Neo4jUtils::createDeviceContact);
    }

    private static void handleInterest(JavaDStream<String> interestLines) {
        JavaDStream<InterestPacket> packets = interestLines.map(InterestPacket::new);
        JavaDStream<Device> devices = packets.map(p -> new Device(p.getIp(), p.getId()));
        JavaDStream<Device> contactDevices =
                devices.flatMap(d -> Neo4jUtils.getDeviceContacts(d).iterator());
        forEach(
                contactDevices,
                e -> {
                    String msg = String.format("o,%s\n", e.getIp());
                    SocketUtils.sendMessage(msg);
                });
    }

    private static <M> void forEach(JavaDStream<M> stream, Consumer<M> consumer) {
        stream.foreachRDD(
                rdd -> {
                    if (!rdd.isEmpty()) {
                        rdd.collect().forEach(consumer);
                    }
                });
    }
}
