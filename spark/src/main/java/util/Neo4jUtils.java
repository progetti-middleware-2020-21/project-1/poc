package util;

import dto.Device;
import dto.DeviceContact;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.neo4j.driver.Driver;
import org.neo4j.driver.GraphDatabase;
import org.neo4j.driver.Result;
import org.neo4j.driver.Session;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@UtilityClass
public class Neo4jUtils {
    private final String NEO4J_URL = "bolt://neo4j:7687";
    private final Driver driver = GraphDatabase.driver(NEO4J_URL);

    public void createDeviceNode(Device dev) {
        String query = "MERGE (n:Device {ip: $ip, id: $id})";
        Map<String, Object> props = new HashMap<>();
        props.put("ip", dev.getIp());
        props.put("id", dev.getId());

        try (Session session = driver.session()) {
            session.run(query, props);
        }
    }

    public void createDeviceContact(DeviceContact devCont) {
        String query =
                String.join(
                        "\n",
                        "MATCH (a:Device{id: $id1})",
                        "MATCH (b:Device{id: $id2})",
                        "MERGE (a)-[r:CONTACT]-(b)",
                        "MERGE (b)-[t:CONTACT]-(a)");
        Map<String, Object> props = new HashMap<>();
        props.put("id1", devCont.getId1());
        props.put("id2", devCont.getId2());

        try (Session session = driver.session()) {
            session.run(query, props);
        }
    }

    public List<Device> getDeviceContacts(Device device) {
        List<Device> ret;
        String query =
                String.join(
                        "\n",
                        "MATCH ({id: $id})-[*]-(conn)",
                        "RETURN DISTINCT properties(conn) AS props");
        try (Session session = driver.session()) {
            Result res = session.run(query, Map.of("id", device.getId()));
            ret =
                    res.stream()
                            .map(r -> new Device(r.get("props").asMap()))
                            .collect(Collectors.toList());
        }
        return ret;
    }

    public void closeDriver() {
        driver.close();
    }
}
