package util;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

@Slf4j
@UtilityClass
public class SocketUtils {
    public void sendMessage(String message) {
        try (Socket sock = new Socket("bridge", 1236)) {
            sock.getOutputStream().write(message.getBytes(StandardCharsets.UTF_8));
            sock.getOutputStream().flush();
        } catch (IOException e) {
            log.error("", e);
        }
    }
}
