#!/usr/bin/env python3

import queue
import threading
import socket
import socketserver
import time

UDP_PORT=1235
TCP_PORT_SEND=1235
TCP_PORT_RECV=1236


DATA_QUEUE=queue.Queue()


def run_udp_server():
    class UDPHandler(socketserver.ThreadingMixIn,
                     socketserver.BaseRequestHandler):
        def handle(self):
            data = self.request[0]
            #socket = self.request[1]
            #sip, sport = self.client_address
            DATA_QUEUE.put(data)

    class UDP6Server(socketserver.UDPServer):
        address_family = socket.AF_INET6

    def run_server():
        with UDP6Server(('', UDP_PORT), UDPHandler) as server:
            server.serve_forever()

    server_thread = threading.Thread(target=run_server)
    server_thread.start()


def run_tcp_server_send():
    tcp_clients=[]
    def send_packets():
        while True:
            data = DATA_QUEUE.get()
            while len(tcp_clients) == 0:
                time.sleep(1000)
            for client in tcp_clients:
                try:
                    client[0].sendall(data)
                except BrokenPipeError:
                    client[0].close()
                    tcp_clients.remove(client)

    def run_server():
        with socket.create_server(('', TCP_PORT_SEND),
                                  family=socket.AF_INET,
                                  reuse_port=True) as sock:
            while True:
                tcp_clients.append(sock.accept())
    handle_tcp = threading.Thread(target=send_packets)
    server_thread = threading.Thread(target=run_server)
    handle_tcp.start()
    server_thread.start()


def run_tcp_server_recv():
    class RecvTCPHandler(socketserver.ThreadingMixIn,
                         socketserver.BaseRequestHandler):
        def handle(self):
            data = self.request.recv(1024)
            data_parts = data.decode().split(',', 2)
            if(data_parts[0] == 'o'):
                local_ip = data_parts[1].strip()
                remote_ip = local_ip.replace('fe80', 'fd00', 1)
                with socket.socket(socket.AF_INET6, socket.SOCK_DGRAM) as sock:
                    sock.connect((remote_ip, 1235))
                    sock.sendall(b'n')

    def run_server():
        with socketserver.TCPServer(('', TCP_PORT_RECV),
                                    RecvTCPHandler) as server:
            server.serve_forever()
    server_thread = threading.Thread(target=run_server)
    server_thread.start()


def main():
    run_udp_server()
    run_tcp_server_send()
    run_tcp_server_recv()
    while True:
        time.sleep(10 * 1000)


if __name__ == "__main__":
    main()
