#include "contiki.h"
#include "lib/random.h"
#include "sys/ctimer.h"
#include "net/ipv6/uip.h"
#include "net/ipv6/uiplib.h"
#include "net/ipv6/uip-ds6.h"
#include "net/ipv6/uip-ds6-route.h"
#include "net/ipv6/uip-debug.h"
#include "simple-udp.h"
#include "dev/button-sensor.h"
#include "leds.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define UDP_PING_PORT 1234
#define UDP_NOTIFY_PORT 1235

#define BROADCAST_TIME (20 + (random_rand() % 20)) * CLOCK_SECOND
#define IPV6_MAX_STR UIPLIB_IPV6_MAX_STR_LEN


static struct simple_udp_connection bcast_conn;
static struct ctimer bcast_timer;
static uip_ipaddr_t bcast_addr;

static char buf_own[IPV6_MAX_STR] = {0};


#define NOTIFY_SERVER "fd00::1"
static uip_ipaddr_t notify_addr;
static struct simple_udp_connection notifyr_conn;
static struct simple_udp_connection notifys_conn;


static void send_broadcast(void *data) {
    printf("Sending broadcast\n");
    simple_udp_sendto(&bcast_conn, "Ping", 4, &bcast_addr);
    ctimer_set(&bcast_timer, BROADCAST_TIME, send_broadcast, NULL);
}


static void bcast_receiver(struct simple_udp_connection *c, const uip_ipaddr_t *source_addr,
                           uint16_t source_port, const uip_ipaddr_t *dest_addr,
                           uint16_t dest_port, const uint8_t *data, uint16_t datalen) {
    char buf_src[IPV6_MAX_STR];
    uiplib_ipaddr_snprint(buf_src, sizeof(buf_src), source_addr);
    printf("Node with IP %s received data from %s\n", buf_own, buf_src);
    char* msg = (char *) malloc(128);
    memset(msg, 0, 128);
    sprintf(msg, "c,%s,%s\n", buf_own, buf_src);
    simple_udp_sendto(&notifys_conn, msg, strlen(msg), &notify_addr);
    free(msg);
}


static void notify_receiver(struct simple_udp_connection *c, const uip_ipaddr_t *source_addr,
                            uint16_t source_port, const uip_ipaddr_t *dest_addr,
                            uint16_t dest_port, const uint8_t *data, uint16_t datalen) {
    char buf_src[IPV6_MAX_STR];
    uiplib_ipaddr_snprint(buf_src, sizeof(buf_src), source_addr);
    printf("Notify: Node with IP %s received data from %s\n", buf_own, buf_src);
    leds_on(LEDS_RED);
}


PROCESS(ct_process, "Contact Tracing IOT");
AUTOSTART_PROCESSES(&ct_process);


PROCESS_THREAD(ct_process, ev, data) {
    PROCESS_BEGIN();
    
    uiplib_ip6addrconv(NOTIFY_SERVER, &notify_addr);
    
    // Retrieve own ip address
    uip_ds6_addr_t* addr;
    addr = uip_ds6_get_link_local(ADDR_PREFERRED);
    uiplib_ipaddr_snprint(buf_own, sizeof(buf_own), &(addr->ipaddr));
    
    
    // Set up broadcast send/receive
    uip_create_linklocal_allnodes_mcast(&bcast_addr);
    simple_udp_register(&bcast_conn, UDP_PING_PORT, NULL, UDP_PING_PORT, bcast_receiver);
    ctimer_set(&bcast_timer, BROADCAST_TIME, send_broadcast, NULL);
    
    // Set up notify socket
    simple_udp_register(&notifyr_conn, UDP_NOTIFY_PORT, NULL, 0, notify_receiver);
    simple_udp_register(&notifys_conn, 0, NULL, UDP_NOTIFY_PORT, NULL);
    
    // Button Handling
    SENSORS_ACTIVATE(button_sensor);
    while(1) {
        PROCESS_YIELD();
        if(ev == sensors_event && data == &button_sensor) {
            leds_on(LEDS_GREEN);
            char* msg = (char *) malloc(64); memset(msg, 0, 64);
            sprintf(msg, "i,%s\n", buf_own);
            simple_udp_sendto(&notifys_conn, msg, strlen(msg), &notify_addr);
            free(msg);
        }
    }

    PROCESS_END();
}
