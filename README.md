# Project 1 (Contact Tracing with Body-worn IoT Devices)

## Repository structure
The proposed solution contains:
* the Contiki-ng application for the devices to detect near devices
* the Spark stream application which receive the various devices pings and compute the distances
* a bridge for Contiki-ng border router and Spark

## Implementation
For this project two technologies were used, Contiki-ng to as a base for MCU programming, together with the use of Cooja for network simulation and Spark for backend data processing: 
* The applications run Contiki-ng on sky motes, needed for detecting nearby devices, report button press (interest event signaling) and drive LEDs to signal contact 
* Python is used in the Bridge component to accommodate the different message systems (UDP for Contiki, TCP for Spark) and different message deliveries (UDP server to receive messages from the Wireless network and TCP server to allow Spark Processing) 
* The Spark cluster receives the device messages, parses them and uses them to: 
* Construct a graph diagram of which devices have been in contact with each other 
* Query the graph diagram in case of an event of interest to get all the nodes that have been in contact with the interested node. 
* Neo4J is used as a backend for storing the graph data and retrieving the node information 

### Contiki-ng

The devices are Sky motes that run Contiki-ng, their role is to: 

* Send ping packets to 1-hop distance devices to discover contact 
* Wait for button input in order to send an interest event 
* Display a red/green LED in case of an event of interest 

All devices communicate via UDP, this is to minimize resource usage, both in terms of RAM/ROM (which is beneficial since worn IOT devices do not have many resources) and in terms of network efficiency (no heartbeats/syn/acks to deal with over the air). 

The ping part is implemented via an UDP packet on port 1234, with a fixed-string payload, which is sent at a random interval in the range of 20-40 seconds. This is to avoid overlaps of pings and guarantee a delivery without resends (which use more power). 

Once a ping is received by a device, it sends a message to the bridge of a contact. Moreover, if the button is pressed another message is sent to the bridge that an event of interest has occurred, and the green LED is turned on (as a way to give visual feedback that the button has been pressed). 

The messages are in ascii format, and follow the structure: 
```
<1-char opcode>,<param 1>,<param 2>,…,<param n> 
```

This is to simplify the parsing on the MCU side, without the use of other message methods (eg. JSON) that would incur in parsing overhead. 

If a message is received via UDP on port 1235, it is interpreted that the node is involved in an event of interest and turns on the red LED. 

### Bridge 

The bridge is a python script that does a few things, mainly: 

* Listening on UDP port 1235 for contact or interest messages from the wireless network. These are put in a queue. 
* Listening on TCP port 1235 where the messages from the queue above are retrieved and sent to all connected clients for processing 
* Listening on TCP port 1236, this is for the Spark cluster to notify that a node is involved in an event of interest. This message is then parsed by the bridge and a notification message is sent to the correct node in the wireless network. 

### Spark & Neo4J 

Spark is used for backend processing, by using Spark Streams. The stream is created with a socket source from the bridge service. Once the messages are received in their micro-batches they are filtered by opcode, parsed and then used for 2 purposes: 

* Contact packets are used to create a graph representation of the contacts, via the use of the Neo4j graph database. Incrementally each node is added to the web of contacts of one device. 
* If an event of interest packet is received, then the graph is queried to get all the other nodes that had a contact with the affected node, and then a message is sent to each of them to notify the event of interest.

## Start the Demo environment
In order to startup the environment, some manual operation are required in order to setup the Contiki-ng environment.

* Start the bridge
  ```
  python bridge/bridge.py
  ```
* Start the Spark environment
  ```
  docker-compose up
  ```
* Start Cooja environment
  ```
  cd <cooja directory>
  ant run
  ```
  Inside Cooja there needs to be at least 2 nodes:
  * One Sky rpl-border-router (from the examples of contiki-ng) for communication
    with the host machine
  * One or more Sky `contiki/contact_tracing.c` nodes, for simulating the actual devices.
* From Cooja expose the `rpl-border-router` serial port as a server and (from the rpl-border-router directory) run
  ```
  make TARGET=cooja connect-router-cooja
  ```
  This will initialize the host-side networking
* Once all of this is done, the simulation can be started inside Cooja
